package com.example.personaltodolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.LinearLayout;

public class ReminderCategoryList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_category_list);

        LinearLayout mainLayout = findViewById(R.id.mainCategoryLayout);

        SingletonDbHandler.getInstance().getReminderByCategories(mainLayout,this);
    }
}
