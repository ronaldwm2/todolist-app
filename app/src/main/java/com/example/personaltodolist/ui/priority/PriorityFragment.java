package com.example.personaltodolist.ui.priority;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.personaltodolist.AddReminder;
import com.example.personaltodolist.PriorityReminderListActivity;
import com.example.personaltodolist.R;
import com.example.personaltodolist.SingletonDataFlow;
import com.example.personaltodolist.SingletonDbHandler;

public class PriorityFragment extends Fragment {

    private PriorityViewModel priorityViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //hide add task
        SingletonDataFlow.getInstance().fabId.setVisibility(View.INVISIBLE);

        priorityViewModel =
                ViewModelProviders.of(this).get(PriorityViewModel.class);
        View root = inflater.inflate(R.layout.fragment_priority, container, false);
//        final TextView textView = root.findViewById(R.id.text_gallery);
//        priorityViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        Button btn_IU = (Button) root.findViewById(R.id.btn_IU);
        Button btn_NIU = (Button) root.findViewById(R.id.btn_NIU);
        Button btn_INU = (Button) root.findViewById(R.id.btn_INU);
        Button btn_NINU = (Button) root.findViewById(R.id.btn_NINU);

        btn_IU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonDataFlow.getInstance().priorityStatus = 0;
                startActivity(new Intent(getActivity(), PriorityReminderListActivity.class));
            }
        });
        btn_NIU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonDataFlow.getInstance().priorityStatus = 1;
                startActivity(new Intent(getActivity(), PriorityReminderListActivity.class));
            }
        });
        btn_INU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonDataFlow.getInstance().priorityStatus = 2;
                startActivity(new Intent(getActivity(), PriorityReminderListActivity.class));
            }
        });
        btn_NINU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonDataFlow.getInstance().priorityStatus = 3;
                startActivity(new Intent(getActivity(), PriorityReminderListActivity.class));
            }
        });
        return root;
    }
}