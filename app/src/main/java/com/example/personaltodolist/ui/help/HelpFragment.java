package com.example.personaltodolist.ui.help;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.personaltodolist.AddReminder;
import com.example.personaltodolist.R;
import com.example.personaltodolist.SingletonDataFlow;
import com.example.personaltodolist.ui.history.HistoryViewModel;
import com.google.android.material.snackbar.Snackbar;

public class HelpFragment extends Fragment {

    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_help, container, false);

        SingletonDataFlow.getInstance().fabId.setVisibility(View.INVISIBLE);




        return root;
    }
}
