package com.example.personaltodolist.ui.home;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.example.personaltodolist.AddReminder;
import com.example.personaltodolist.R;
import com.example.personaltodolist.SingletonDataFlow;
import com.example.personaltodolist.SingletonDbHandler;
import com.example.personaltodolist.model.Account;
import com.example.personaltodolist.model.Reminder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.fragment_home, container, false);

        SingletonDataFlow.getInstance().fabId.setVisibility(View.VISIBLE);

        SingletonDataFlow.getInstance().fabId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Snackbar.make(v, "Dari Home Fragment", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            startActivity(new Intent(getActivity(), AddReminder.class));
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        LinearLayout mainLayout = root.findViewById(R.id.main_home_layout);
        mainLayout.removeAllViews();
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        SingletonDbHandler.getInstance().readReminderOnce(mainLayout,getContext(), alarmManager);
    }


}