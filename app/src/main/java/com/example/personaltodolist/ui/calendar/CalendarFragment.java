package com.example.personaltodolist.ui.calendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.personaltodolist.R;
import com.example.personaltodolist.SingletonDataFlow;
import com.example.personaltodolist.SingletonDbHandler;
import com.example.personaltodolist.model.Categorized_Reminder;
import com.example.personaltodolist.model.Reminder;
import com.squareup.timessquare.CalendarPickerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CalendarFragment extends Fragment {

    private CalendarViewModel calendarViewModel;
    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        SingletonDataFlow.getInstance().fabId.setVisibility(View.INVISIBLE);

        calendarViewModel =
                ViewModelProviders.of(this).get(CalendarViewModel.class);
        root = inflater.inflate(R.layout.fragment_tools, container, false);


        Date today = Calendar.getInstance().getTime();
        Calendar getMaxDate = Calendar.getInstance();
        Calendar getMinDate = Calendar.getInstance();
        getMinDate.set(Calendar.DAY_OF_MONTH,Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
        getMinDate.set(Calendar.HOUR_OF_DAY,0);
        getMinDate.set(Calendar.MINUTE,0);
        getMinDate.set(Calendar.SECOND,0);

        getMaxDate.set(Calendar.DAY_OF_MONTH,Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH) + 1);
        getMaxDate.set(Calendar.HOUR_OF_DAY,23);
        getMaxDate.set(Calendar.MINUTE,59);
        getMaxDate.set(Calendar.SECOND,59);

        Date maxDate = getMaxDate.getTime();
        Date minDate = getMinDate.getTime();

        CalendarPickerView picker = root.findViewById(R.id.calendar);
        picker.init(minDate,maxDate).withSelectedDate(today);

        picker.clearHighlightedDates();

        ArrayList<Date> dates = new ArrayList<>();
        for(Categorized_Reminder cat_rem : SingletonDbHandler.getInstance().lstOfCategorizedReminder) {
            for(Reminder rem : cat_rem.getLstOfReminder()){
                if(rem.getReminderDate().compareTo(maxDate) < 0 && rem.getReminderDate().compareTo(minDate) > 0) {
                    dates.add(rem.getReminderDate());
                }
            }
        }
        picker.highlightDates(dates);

        final LinearLayout mainLayout = root.findViewById(R.id.calendar_main_layout);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMMM-yyyy");
        SingletonDbHandler.getInstance().getReminderByDates(mainLayout,getContext(),dateFormatter.format(today),dateFormatter);

        picker.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMMM-yyyy");
                SingletonDbHandler.getInstance().getReminderByDates(mainLayout,getContext(),dateFormatter.format(date),dateFormatter);
            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

        return root;
    }
}