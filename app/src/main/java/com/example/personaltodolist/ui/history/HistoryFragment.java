package com.example.personaltodolist.ui.history;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.personaltodolist.AddReminder;
import com.example.personaltodolist.R;
import com.example.personaltodolist.SingletonDataFlow;
import com.example.personaltodolist.SingletonDbHandler;
import com.google.android.material.snackbar.Snackbar;

public class HistoryFragment extends Fragment {

    private HistoryViewModel historyViewModel;
    private View root;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        historyViewModel =
                ViewModelProviders.of(this).get(HistoryViewModel.class);
        root = inflater.inflate(R.layout.fragment_slideshow, container, false);

        SingletonDataFlow.getInstance().fabId.setVisibility(View.INVISIBLE);

        SingletonDataFlow.getInstance().fabId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Dari Home Fragment", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                startActivity(new Intent(getActivity(), AddReminder.class));
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        LinearLayout mainLayout = root.findViewById(R.id.main_history_layout);
        mainLayout.removeAllViews();
        SingletonDbHandler.getInstance().readHistoryOnce(mainLayout,getContext());
    }
}