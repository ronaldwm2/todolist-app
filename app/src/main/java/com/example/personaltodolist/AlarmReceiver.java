package com.example.personaltodolist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {
    public AlarmReceiver(){
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent (context,NotificationServices.class);
        service.putExtra("taskname",intent.getExtras().get("taskname").toString());
        service.putExtra("taskpriority",intent.getExtras().get("taskpriority").toString());
        service.putExtra("intentCode",intent.getExtras().get("intentCode").toString());
        service.putExtra("fbkeycode", intent.getExtras().get("fbkeycode").toString());

        Log.d("keycode",intent.getExtras().get("fbkeycode").toString());

        Log.d("AlarmReceiver","Broadcast Received");
        context.startService(service);
    }
}
