package com.example.personaltodolist.model;

import java.util.ArrayList;

public class Categorized_Reminder {
    ArrayList<Reminder> lstOfReminder;
    String category;

    public ArrayList<Reminder> getLstOfReminder() {
        return lstOfReminder;
    }

    public void setLstOfReminder(ArrayList<Reminder> lstOfReminder) {
        this.lstOfReminder = lstOfReminder;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Categorized_Reminder() {
    }

    public Categorized_Reminder(ArrayList<Reminder> lstOfReminder, String category) {
        this.lstOfReminder = lstOfReminder;
        this.category = category;
    }
}
