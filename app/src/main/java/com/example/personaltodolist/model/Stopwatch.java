package com.example.personaltodolist.model;

public class Stopwatch {
    String time;

    public Stopwatch(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public Stopwatch() {
    }

    public void setTime(String time) {
        this.time = time;
    }
}
