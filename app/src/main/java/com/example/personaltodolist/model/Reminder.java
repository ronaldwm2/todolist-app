package com.example.personaltodolist.model;

import com.example.personaltodolist.SingletonDataFlow;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@IgnoreExtraProperties
public class Reminder implements Serializable {
    String key;
    Date reminderDate;
//    Date reminderTime;
    String priority;
    String category;
    String detail;
    String nameTask;
    String id;
    public transient List<Stopwatch> stopwatchData = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
//    public Date getReminderTime() {
//        return reminderTime;
//    }
//
//    public void setReminderTime(Date reminderTime) {
//        this.reminderTime = reminderTime;
//    }

    public String getNameTask() {
        return nameTask;
    }

    public void setNameTask(String nameTask) {
        this.nameTask = nameTask;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(Date reminderDate) {
        this.reminderDate = reminderDate;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }


    public Reminder() {
        this.id = SingletonDataFlow.getInstance().loggedInId;
    }

    public Reminder(Date reminderDate, String detail) {
        this.reminderDate = reminderDate;
        this.detail = detail;
        this.id = SingletonDataFlow.getInstance().loggedInId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public Reminder(
            Date reminderDate,
            //Date reminderTime,
            String priority,
            String category,
            String detail,
            String nameTask) {
        this.reminderDate = reminderDate;
        this.priority = priority;
        this.category = category;
        this.detail = detail;
        this.nameTask = nameTask;
        this.id = SingletonDataFlow.getInstance().loggedInId;
//        this.reminderTime = reminderTime;
    }




}
