package com.example.personaltodolist;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.personaltodolist.model.Reminder;
import com.google.firebase.database.FirebaseDatabase;

public class ConflictReminder extends AppCompatDialogFragment {
    Reminder newReminder;
    FirebaseDatabase db;
    Activity activity;
    ConflictReminder () {}

    ConflictReminder(Reminder newReminder, FirebaseDatabase db, Activity activity){
        super();
        this.newReminder = newReminder;
        this.db = db;
        this.activity = activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(SingletonDataFlow.getInstance().dialogMsg)
                .setTitle("Reminder Conflict")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        db.getInstance().getReference("Reminder").push().setValue(newReminder);
                        Toast.makeText(activity.getApplicationContext(), "Berhasil Menciptakan Reminder Baru", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        activity.finish();
                    }
                });
        AlertDialog dialog = builder.create();
        return dialog;
    }

}
