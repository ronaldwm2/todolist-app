package com.example.personaltodolist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.personaltodolist.model.Reminder;
import com.google.android.material.snackbar.Snackbar;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddReminder extends AppCompatActivity {
    private DatePickerDialog pickerDialog;
    private TimePickerDialog timePickerDialog;
    private Date date;
    private Date time;
    private String prioritySelected;
    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat timeFormatter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);
        SingletonDataFlow.getInstance().supportManager = getSupportFragmentManager();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        final EditText btnAddDate =  findViewById(R.id.btnDate);
        btnAddDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

//        timeFormatter = new SimpleDateFormat("hh:mm a");
//        final Button btnAddTime = (Button) findViewById(R.id.btnTime);
//        btnAddTime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showTimeDialog();
//            }
//        });

        final Spinner priority = (Spinner) findViewById(R.id.spinnerPriority);
        final EditText category = findViewById(R.id.editTextCategory);
        final EditText detail = findViewById(R.id.editTextDetail);
        final EditText nameTask = findViewById(R.id.editTextNameTask);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.priority_list,
                android.R.layout.simple_spinner_dropdown_item
        );
        priority.setAdapter(adapter);
        priority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prioritySelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Button addButton = findViewById(R.id.addButton);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addButton.setVisibility(View.INVISIBLE);
                if(date == null || category.getText().toString().equals("") || detail.getText().toString().equals("") || nameTask.getText().toString().equals("")){
                    Snackbar.make(view, "Tolong isi semua field", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    return;
                }
                AlarmManager alarmManager = (AlarmManager) AddReminder.this.getSystemService(Context.ALARM_SERVICE);
                SingletonDbHandler.getInstance().addReminder(AddReminder.this,new Reminder(date,prioritySelected,category.getText().toString(),detail.getText().toString(), nameTask.getText().toString()));

            }
        });
    }

    private void showDateDialog(){
        Calendar calendar = Calendar.getInstance();

        pickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(i,i1,i2);
                date = newDate.getTime();
                showTimeDialog(newDate);
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        pickerDialog.show();
    }

    private void showTimeDialog(final Calendar calendar){
        //Inisialisasi Time Picker Dialog
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                //Method ini dipanggil saat kita selesai memilih waktu di DatePicker
                calendar.set(Calendar.MINUTE,minute);
                calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                final EditText tv = (EditText) findViewById(R.id.btnDate);
                tv.setText(dateFormatter.format(calendar.getTime()));
                date = calendar.getTime();
            }
        }, //Tampilkan jam saat ini ketika TimePicker pertama kali dibuka
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                //Cek apakah format waktu menggunakan 24-hour format
                DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

}


