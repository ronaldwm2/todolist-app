package com.example.personaltodolist;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.os.Build;
import android.os.Debug;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class NotificationServices extends IntentService {
    private static int NOTIFICATION_ID = 1;
    String remindername,reminderpriority,reminderCode,fbkeyname;

    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        super.onStartCommand(i, startId, startId);
        Log.d("service","test");
        remindername = i.getStringExtra("taskname");
        reminderpriority = i.getStringExtra("taskpriority");
        reminderCode = i.getStringExtra("intentCode");
        fbkeyname = i.getStringExtra("fbkeycode");
        Log.d("keycode",fbkeyname);
        if(reminderCode == ""){
            reminderCode = "1";
        }
        return START_STICKY;
    }

    public void onCreate(){
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent i) {
        Context context = this.getApplicationContext();
        if(fbkeyname == null) {
            fbkeyname = i.getStringExtra("fbkeycode");
        }
        Intent mIntent = new Intent(this,ReminderDetails.class);
        mIntent.putExtra("fbkeycode",fbkeyname);
        Log.d("dronhandleintent",fbkeyname);
        SingletonDbHandler.getInstance().readSingleReminder(fbkeyname);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context,0,mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String channelId = "channel_id";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "test",
                    NotificationManager.IMPORTANCE_HIGH);
            NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(channel);

            NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(context,"channel_id");
            nBuilder
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle(reminderpriority)
                    .setContentText(remindername)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);
            NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);
            managerCompat.notify(NOTIFICATION_ID,nBuilder.build());
        }
        Log.d("Notification Services","Creating New Notification");
    }


    public NotificationServices() {
        super("NotificationServices");
    }

    protected void startJob(Intent intent) {

    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
