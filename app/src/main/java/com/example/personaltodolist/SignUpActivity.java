package com.example.personaltodolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        SingletonDataFlow.getInstance().context = this.getApplicationContext();

        Button registerBtn = findViewById(R.id.registerBtn);
        final EditText idEt = findViewById(R.id.etSignupId);
        final EditText idPw = findViewById(R.id.etSignupPw);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonDbHandler.getInstance().writeNewId(idEt.getText().toString(),idPw.getText().toString());
            }
        });
    }
}
