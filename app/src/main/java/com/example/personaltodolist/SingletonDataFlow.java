package com.example.personaltodolist;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.SubMenu;
import android.view.View;
import android.widget.LinearLayout;

import androidx.fragment.app.FragmentManager;

import com.example.personaltodolist.model.Reminder;
import com.google.android.material.navigation.NavigationView;

//menerapkan singleton karena pada saat kita instansi dalam suatu class, kita tidak perlu lagi membuat object
//atau instansi berulang kali, dalam setiap kelas, kita hanya perlu membuat satu instansi, lalu kita terapkan
//untuk semua kelas

public class SingletonDataFlow {
    private static final SingletonDataFlow ourInstance = new SingletonDataFlow(); //instansiasi singleton

    public static SingletonDataFlow getInstance() {
        return ourInstance;
    } //instansi singleton


    private SingletonDataFlow() {
    }//constructor mode private  agar tidak dapat di instansiasi oleh class lain

    public View fabId;
    public Context context;
    public Boolean successLogin = false;
    public String name;
    public int priorityStatus;
    public SharedPreferences login_info;
    public SharedPreferences.Editor login_info_edit;
    public String loggedInId;
    public Reminder remDetails;
    public int temp = 0;
    public NavigationView navView;
    public SubMenu subMenu;
    public String categories;

    public String dialogMsg;
    public FragmentManager supportManager;
}
