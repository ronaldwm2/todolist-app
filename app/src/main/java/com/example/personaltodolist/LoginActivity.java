package com.example.personaltodolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SingletonDataFlow.getInstance().context = this.getApplicationContext();

        Button loginBtn = findViewById(R.id.loginBtn);
        final EditText idEt = findViewById(R.id.etId);
        final EditText idPw = findViewById(R.id.etPw);
        final TextView tvSignupText = findViewById(R.id.signupText);

        SingletonDataFlow.getInstance().login_info = getSharedPreferences("login_info",MODE_PRIVATE);
        SingletonDataFlow.getInstance().login_info_edit = SingletonDataFlow.getInstance().login_info.edit();

        String checkLoginInfo = SingletonDataFlow.getInstance().login_info.getString("id","");
        if(!checkLoginInfo.equals("")){
            SingletonDataFlow.getInstance().loggedInId = checkLoginInfo;
            Intent i = new Intent(SingletonDataFlow.getInstance().context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

        tvSignupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,SignUpActivity.class));
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonDbHandler.getInstance().login(idEt.getText().toString(),idPw.getText().toString());
                // SingletonDbHandler.getInstance().writeNewId(idEt.getText().toString(),idPw.getText().toString());
                // System.out.println(SingletonDataFlow.getInstance().successLogin);
            }
        });
    }
}
