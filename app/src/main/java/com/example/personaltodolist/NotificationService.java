package com.example.personaltodolist;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class NotificationService extends IntentService {
    private static int NOTIFICATION_ID = 1;
    String remindername,reminderpriority,reminderCode;

    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        remindername = i.getStringExtra("taskname");
        reminderpriority = i.getStringExtra("taskpriority");
        reminderCode = i.getStringExtra("intentCode");
        return START_STICKY;
    }

    public NotificationService(){
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }
}
