package com.example.personaltodolist;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.ViewCompat;

import com.example.personaltodolist.model.Account;
import com.example.personaltodolist.model.Categorized_Reminder;
import com.example.personaltodolist.model.Reminder;
import com.example.personaltodolist.model.Stopwatch;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.ContentValues.TAG;
import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.MODE_APPEND;
import static android.content.Context.MODE_PRIVATE;

public class SingletonDbHandler {
    private static final SingletonDbHandler ourInstance = new SingletonDbHandler();
    public static SingletonDbHandler getInstance() {
        return ourInstance;
    }
    private SingletonDbHandler() {
    }

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    Button currButton;
    int Seconds, Minutes, MilliSeconds ;
    Handler handler;
    int buttonId;

    private FirebaseDatabase db = FirebaseDatabase.getInstance();
    private DatabaseReference dbRef;

    private ArrayList<Account> lstOfAccount = new ArrayList<>();
    public ArrayList<Categorized_Reminder> lstOfCategorizedReminder = new ArrayList<>();
    public ArrayList<Categorized_Reminder> lstOfHistoryCategorizedReminder = new ArrayList<>();
    public ArrayList<Categorized_Reminder> lstOfMenu = new ArrayList<>();

    public DatabaseReference getDbRef() {
        return dbRef;
    }

    public void setDbRef(DatabaseReference dbRef) {
        this.dbRef = dbRef;
    }

    public FirebaseDatabase getDb() {
        return db;
    }

    public void setDb(FirebaseDatabase db) {
        this.db = db;
    }


    public void getAllCategories(final Context context){
        db.getReference("Reminder").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Reminder> lstOfReminder = new ArrayList<>();
                lstOfMenu = new ArrayList<>();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    Reminder reminder = ds.getValue(Reminder.class);
                    if(!reminder.getId().equalsIgnoreCase(SingletonDataFlow.getInstance().loggedInId)) continue;
                    lstOfReminder.add(reminder);
                }
                boolean exist = false;
                for(Reminder rem : lstOfReminder) {
                    for (Categorized_Reminder cat_rem : lstOfMenu) {
                        if (cat_rem.getCategory().equals(rem.getCategory())) {
                            exist = true;
                            cat_rem.getLstOfReminder().add(rem);
                        }
                    }
                    if (!exist) {
                        ArrayList<Reminder> newlstOfReminder = new ArrayList<>();
                        newlstOfReminder.add(rem);
                        lstOfMenu.add(new Categorized_Reminder(newlstOfReminder, rem.getCategory()));
                    }
                }

                if(SingletonDataFlow.getInstance().subMenu != null) {
                    SingletonDataFlow.getInstance().subMenu.clear();
                }

                SingletonDataFlow.getInstance().subMenu = SingletonDataFlow.getInstance().navView.getMenu().addSubMenu("Categories");

                for(Categorized_Reminder cat_rem: lstOfMenu){
                    SingletonDataFlow.getInstance().subMenu.add(cat_rem.getCategory()).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            SingletonDataFlow.getInstance().categories = menuItem.getTitle().toString();
                            Intent i = new Intent(context,ReminderCategoryList.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(i);
                            return true;
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getReminderByCategories(final LinearLayout mainLayout, final Context context) {
        db.getReference("Reminder").addListenerForSingleValueEvent(new ValueEventListener() { // buat ngambil 1x
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lstOfCategorizedReminder.clear();
                getAllCategories(context);
                ArrayList<Reminder> lstOfReminder = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Reminder newReminder = ds.getValue(Reminder.class);
                    if(!newReminder.getId().equalsIgnoreCase(SingletonDataFlow.getInstance().loggedInId)) continue;
                    newReminder.setKey(ds.getKey());

                    for (DataSnapshot dsStopwatch : ds.child("stopwatchData").getChildren()){
                        newReminder.stopwatchData.add(dsStopwatch.getValue(Stopwatch.class));
                    }
                    lstOfReminder.add(newReminder);
                }
                categorizingReminder(lstOfReminder, false);
                mainLayout.removeAllViews();
                LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearParams.setMargins(30,0,30,10);
                LinearLayout newLinearLayout = new LinearLayout(context);
                newLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                newLinearLayout.setLayoutParams(linearParams);
                TextView legend1 = new TextView(context);
                TextView legend2 = new TextView(context);
                TextView legend3 = new TextView(context);
                TextView legend4 = new TextView(context);

                LinearLayout.LayoutParams paramsText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1f);
                paramsText.setMargins(0,0,10,0);
                LinearLayout.LayoutParams paramsTextRight = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1f);
                paramsTextRight.setMargins(10,0,0,0);

                legend1.setText("Important Urgent");
                legend1.setLayoutParams(paramsText);
                legend1.setGravity(Gravity.CENTER);
                legend1.setBackgroundResource(R.drawable.important_urgent_ractangle);

                legend2.setText("Important Not Urgent");
                legend2.setLayoutParams(paramsTextRight);
                legend2.setGravity(Gravity.CENTER);
                legend2.setBackgroundResource(R.drawable.important_not_urgent_ractangle);


                legend3.setText("Not Important Urgent");
                legend3.setLayoutParams(paramsText);
                legend3.setGravity(Gravity.CENTER);
                legend3.setBackgroundResource(R.drawable.not_important_urgent_ractangle);

                legend4.setText("Not Important Not Urgent");
                legend4.setLayoutParams(paramsTextRight);
                legend4.setGravity(Gravity.CENTER);
                legend4.setBackgroundResource(R.drawable.not_important_not_urgent_ractangle);

                newLinearLayout.addView(legend1);
                newLinearLayout.addView(legend2);

                LinearLayout newLinearLayout2 = new LinearLayout(context);
                newLinearLayout2.setOrientation(LinearLayout.HORIZONTAL);
                newLinearLayout2.setLayoutParams(linearParams);

                newLinearLayout2.addView(legend3);
                newLinearLayout2.addView(legend4);

                mainLayout.addView(newLinearLayout);
                mainLayout.addView(newLinearLayout2);

                for(Categorized_Reminder cat_rem : lstOfCategorizedReminder){
                    if(!cat_rem.getCategory().equals(SingletonDataFlow.getInstance().categories)) continue;
                    Collections.sort(cat_rem.getLstOfReminder(), new Comparator<Reminder>() {
                        @Override
                        public int compare(Reminder reminder, Reminder t1) {
                            return reminder.getReminderDate().compareTo(t1.getReminderDate());
                        }
                    });
                    for (Reminder rem : cat_rem.getLstOfReminder()){
                        addToDoList(mainLayout,rem,context);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void addReminder(final Activity activity, final Reminder newReminder) {
        db.getReference("Reminder").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean exist = false;
                boolean isExistInterval = false;
                List<Reminder> lstOfIntervalReminder = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Reminder checkReminder = ds.getValue(Reminder.class);
                    if(checkReminder.getNameTask().equals(newReminder.getNameTask()) && checkReminder.getReminderDate().toString().equals(newReminder.getReminderDate().toString()) &&
                    checkReminder.getCategory().equals(newReminder.getCategory())){
                        exist = true;
                    }
                    SimpleDateFormat format = new SimpleDateFormat("dd MM yyyy hh:mm");
                    long timeDiff = ((checkReminder.getReminderDate().getTime() - newReminder.getReminderDate().getTime()) / (60 * 1000));
                    Log.d(format.format(checkReminder.getReminderDate()),String.valueOf(timeDiff));
                    if (timeDiff > -15 && timeDiff < 15){
                        isExistInterval = true;
                        lstOfIntervalReminder.add(checkReminder);
                    }
                }
                if(exist){
                    Toast.makeText(activity.getApplicationContext(),"Ada task dengan nama, waktu, dan kategori yang sama",Toast.LENGTH_SHORT).show();
                    activity.finish();
                    return;
                } else {
                    if(isExistInterval){
                        SingletonDataFlow.getInstance().dialogMsg = "Conflict Reminder, proceed?\n";
                        SimpleDateFormat df = new SimpleDateFormat("hh:mm");
                        for (Reminder intervalReminder: lstOfIntervalReminder) {
                            SingletonDataFlow.getInstance().dialogMsg = SingletonDataFlow.getInstance().dialogMsg.concat(intervalReminder.getNameTask()) + " " + df.format(intervalReminder.getReminderDate()) + "\n";
                        }
                        ConflictReminder conflictReminder = new ConflictReminder(newReminder,db,activity);
                        conflictReminder.show(SingletonDataFlow.getInstance().supportManager, "test");
                    } else {
                        db.getInstance().getReference("Reminder").push().setValue(newReminder);
                        Toast.makeText(activity.getApplicationContext(), "Berhasil Menciptakan Reminder Baru", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addHistoryReminder(Reminder newReminder, String key, final Activity activity) {
        DatabaseReference newRef = db.getInstance().getReference("Reminder_History").push();
        newRef.setValue(newReminder);
        for (Stopwatch sw : newReminder.stopwatchData) {
            db.getInstance().getReference("Reminder_History").child(newRef.getKey()).child("stopwatchData").push().setValue(sw);
        }
        db.getInstance().getReference("Reminder").child(key).removeValue().addOnSuccessListener(activity, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(activity,"Task Telah Selesai",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(activity.getApplicationContext(), MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(i);
            }
        });
    }

    public void filterReminderByImportance(final LinearLayout mainLayout, final Context context) {
        db.getReference("Reminder").addListenerForSingleValueEvent(new ValueEventListener() { // buat ngambil 1x
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lstOfCategorizedReminder.clear();
                ArrayList<Reminder> lstOfReminder = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Reminder newReminder = ds.getValue(Reminder.class);

                    if(!newReminder.getId().equalsIgnoreCase(SingletonDataFlow.getInstance().loggedInId)) continue;
                    newReminder.setKey(ds.getKey());
                    if(SingletonDataFlow.getInstance().priorityStatus == 0) {
                        if(newReminder.getPriority().equals("Important Urgent"))  lstOfReminder.add(newReminder);
                    } else if (SingletonDataFlow.getInstance().priorityStatus == 1) {
                        if(newReminder.getPriority().equals("Not Important Urgent"))  lstOfReminder.add(newReminder);
                    } else if (SingletonDataFlow.getInstance().priorityStatus == 2) {
                        if(newReminder.getPriority().equals("Important Not Urgent"))  lstOfReminder.add(newReminder);
                    } else {
                        if(newReminder.getPriority().equals("Not Important Not Urgent"))  lstOfReminder.add(newReminder);
                    }
                }
                categorizingReminder(lstOfReminder,false);
                for(Categorized_Reminder cat_rem : lstOfCategorizedReminder){
                    TextView newTv = new TextView(context);
                    newTv.setGravity(Gravity.CENTER);
                    newTv.setText(cat_rem.getCategory());
                    newTv.setTextColor(Color.parseColor("#FFFFFF"));
                    mainLayout.addView(newTv);
                    for (Reminder rem : cat_rem.getLstOfReminder()){
                        addToDoList(mainLayout,rem,context);
                    }
                }
                //akan tampil bila tidak ada task di menu important
                if(lstOfReminder.size() == 0) {
                    LinearLayout lin = new LinearLayout(context);
                    LinearLayout.LayoutParams full = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    TextView noRecord = new TextView(context);
                    noRecord.setText("No Schedule Task");
                    noRecord.setGravity(Gravity.CENTER);
                    noRecord.setTextColor(Color.WHITE);
                    noRecord.setTextSize(36);
                    LinearLayout.LayoutParams noRecords = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    noRecords.topMargin = 24;
                    noRecord.setLayoutParams(noRecords);
                    lin.setLayoutParams(full);
                    lin.setGravity(Gravity.CENTER);
                    lin.addView(noRecord);
                    mainLayout.addView(lin);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void readHistoryOnce(final LinearLayout mainLayout, final Context context){
        db.getReference("Reminder_History").addListenerForSingleValueEvent(new ValueEventListener() { // buat ngambil 1x
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lstOfHistoryCategorizedReminder.clear();
                ArrayList<Reminder> lstOfHistoryReminder = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Reminder newReminder = ds.getValue(Reminder.class);
                    if(!newReminder.getId().equalsIgnoreCase(SingletonDataFlow.getInstance().loggedInId)) continue;
                    newReminder.setKey(ds.getKey());

                    for (DataSnapshot dsStopwatch : ds.child("stopwatchData").getChildren()){
                        newReminder.stopwatchData.add(dsStopwatch.getValue(Stopwatch.class));
                    }

                    Calendar calendar = Calendar.getInstance();

                    lstOfHistoryReminder.add(newReminder);
                }

                categorizingReminder(lstOfHistoryReminder,true);
                mainLayout.removeAllViews();
                LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearParams.setMargins(30,0,30,10);
                LinearLayout newLinearLayout = new LinearLayout(context);
                newLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                newLinearLayout.setLayoutParams(linearParams);
                TextView legend1 = new TextView(context);
                TextView legend2 = new TextView(context);
                TextView legend3 = new TextView(context);
                TextView legend4 = new TextView(context);

                LinearLayout.LayoutParams paramsText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1f);
                paramsText.setMargins(0,0,10,0);
                LinearLayout.LayoutParams paramsTextRight = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1f);
                paramsTextRight.setMargins(10,0,0,0);

                legend1.setText("Important Urgent");
                legend1.setLayoutParams(paramsText);
                legend1.setGravity(Gravity.CENTER);
                legend1.setBackgroundResource(R.drawable.important_urgent_ractangle);

                legend2.setText("Important Not Urgent");
                legend2.setLayoutParams(paramsTextRight);
                legend2.setGravity(Gravity.CENTER);
                legend2.setBackgroundResource(R.drawable.important_not_urgent_ractangle);


                legend3.setText("Not Important Urgent");
                legend3.setLayoutParams(paramsText);
                legend3.setGravity(Gravity.CENTER);
                legend3.setBackgroundResource(R.drawable.not_important_urgent_ractangle);

                legend4.setText("Not Important Not Urgent");
                legend4.setLayoutParams(paramsTextRight);
                legend4.setGravity(Gravity.CENTER);
                legend4.setBackgroundResource(R.drawable.not_important_not_urgent_ractangle);

                newLinearLayout.addView(legend1);
                newLinearLayout.addView(legend2);

                LinearLayout newLinearLayout2 = new LinearLayout(context);
                newLinearLayout2.setOrientation(LinearLayout.HORIZONTAL);
                newLinearLayout2.setLayoutParams(linearParams);

                newLinearLayout2.addView(legend3);
                newLinearLayout2.addView(legend4);

                mainLayout.addView(newLinearLayout);
                mainLayout.addView(newLinearLayout2);
                for(Categorized_Reminder cat_rem : lstOfHistoryCategorizedReminder){
                    TextView newTv = new TextView(context);
                    newTv.setGravity(Gravity.CENTER);
                    newTv.setText(cat_rem.getCategory());
                    newTv.setTextColor(Color.parseColor("#FFFFFF"));
                    mainLayout.addView(newTv);
                    for (Reminder rem : cat_rem.getLstOfReminder()){
                        addToDoHistoryList(mainLayout,rem,context);
                    }
                }
                //nampilin pemberitahuan bila belum ada task di history
                if(lstOfHistoryReminder.size() == 0) {
                    LinearLayout lin = new LinearLayout(context);
                    LinearLayout.LayoutParams full = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    TextView noRecord = new TextView(context);
                    noRecord.setText("No Schedule History Task");
                    noRecord.setGravity(Gravity.CENTER);
                    noRecord.setTextColor(Color.WHITE);
                    noRecord.setTextSize(36);
                    LinearLayout.LayoutParams noRecords = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    noRecords.topMargin = 24;
                    noRecord.setLayoutParams(noRecords);
                    lin.setLayoutParams(full);
                    lin.setGravity(Gravity.CENTER);
                    lin.addView(noRecord);
                    mainLayout.addView(lin);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void readSingleReminder(final String key){
        db.getReference("Reminder").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Reminder newReminder = dataSnapshot.child(key).getValue(Reminder.class);
                for (DataSnapshot dsStopwatch : dataSnapshot.child("stopwatchData").getChildren()){
                    newReminder.stopwatchData.add(dsStopwatch.getValue(Stopwatch.class));
                }
                SingletonDataFlow.getInstance().remDetails = newReminder;
                SingletonDataFlow.getInstance().remDetails.setKey(key);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
        //Calendar
    public void getReminderByDates(final LinearLayout mainLayout, final Context context, final String today, final SimpleDateFormat formatter) {
        db.getReference("Reminder").addListenerForSingleValueEvent(new ValueEventListener() { // buat ngambil 1x
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lstOfCategorizedReminder.clear();
                getAllCategories(context);
                ArrayList<Reminder> lstOfReminder = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Reminder newReminder = ds.getValue(Reminder.class);
                    if(!newReminder.getId().equalsIgnoreCase(SingletonDataFlow.getInstance().loggedInId)) continue;
                    if(!formatter.format(newReminder.getReminderDate()).equals(today)) continue;
                    newReminder.setKey(ds.getKey());

                    for (DataSnapshot dsStopwatch : ds.child("stopwatchData").getChildren()){
                        newReminder.stopwatchData.add(dsStopwatch.getValue(Stopwatch.class));
                    }
                    lstOfReminder.add(newReminder);
                }
                categorizingReminder(lstOfReminder, false);
                mainLayout.removeAllViews();
                LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearParams.setMargins(30,0,30,10);
                LinearLayout newLinearLayout = new LinearLayout(context);
                newLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                newLinearLayout.setLayoutParams(linearParams);
                TextView legend1 = new TextView(context);
                TextView legend2 = new TextView(context);
                TextView legend3 = new TextView(context);
                TextView legend4 = new TextView(context);

                LinearLayout.LayoutParams paramsText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1f);
                paramsText.setMargins(0,0,10,0);
                LinearLayout.LayoutParams paramsTextRight = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1f);
                paramsTextRight.setMargins(10,0,0,0);

                legend1.setText("Important Urgent");
                legend1.setLayoutParams(paramsText);
                legend1.setGravity(Gravity.CENTER);
                legend1.setBackgroundResource(R.drawable.important_urgent_ractangle);

                legend2.setText("Important Not Urgent");
                legend2.setLayoutParams(paramsTextRight);
                legend2.setGravity(Gravity.CENTER);
                legend2.setBackgroundResource(R.drawable.important_not_urgent_ractangle);


                legend3.setText("Not Important Urgent");
                legend3.setLayoutParams(paramsText);
                legend3.setGravity(Gravity.CENTER);
                legend3.setBackgroundResource(R.drawable.not_important_urgent_ractangle);

                legend4.setText("Not Important Not Urgent");
                legend4.setLayoutParams(paramsTextRight);
                legend4.setGravity(Gravity.CENTER);
                legend4.setBackgroundResource(R.drawable.not_important_not_urgent_ractangle);

                newLinearLayout.addView(legend1);
                newLinearLayout.addView(legend2);

                LinearLayout newLinearLayout2 = new LinearLayout(context);
                newLinearLayout2.setOrientation(LinearLayout.HORIZONTAL);
                newLinearLayout2.setLayoutParams(linearParams);

                newLinearLayout2.addView(legend3);
                newLinearLayout2.addView(legend4);

                mainLayout.addView(newLinearLayout);
                mainLayout.addView(newLinearLayout2);
                for(Categorized_Reminder cat_rem : lstOfCategorizedReminder){

                    TextView newTv = new TextView(context);
                    newTv.setGravity(Gravity.CENTER);
                    newTv.setText(cat_rem.getCategory());
                    newTv.setTextSize(24);
                    newTv.setTextColor(Color.parseColor("#FFFFFF"));
                    mainLayout.addView(newTv);

                    Collections.sort(cat_rem.getLstOfReminder(), new Comparator<Reminder>() {
                        @Override
                        public int compare(Reminder reminder, Reminder t1) {
                            return reminder.getReminderDate().compareTo(t1.getReminderDate());
                        }
                    });
                    for (Reminder rem : cat_rem.getLstOfReminder()){
                        addToDoList(mainLayout,rem,context);
                    }
                }

                //Nampilin ini bila tidak ada task
                if(lstOfReminder.size() == 0) {
                    LinearLayout lin = new LinearLayout(context);
                    LinearLayout.LayoutParams full = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    TextView noRecord = new TextView(context);
                    noRecord.setText("No Schedule Task");
                    noRecord.setGravity(Gravity.CENTER);
                    noRecord.setTextColor(Color.WHITE);
                    noRecord.setTextSize(36);
                    LinearLayout.LayoutParams noRecords = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    noRecords.topMargin = 24;
                    noRecord.setLayoutParams(noRecords);
                    lin.setLayoutParams(full);
                    lin.setGravity(Gravity.CENTER);
                    lin.addView(noRecord);
                    mainLayout.addView(lin);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void clearAllReminderNotification(final AlarmManager alarmManager, final Context context){
        db.getReference("Reminder").addListenerForSingleValueEvent(new ValueEventListener() { // buat ngambil 1x
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Reminder> lstOfReminder = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Reminder newReminder = ds.getValue(Reminder.class);
                    newReminder.setKey(ds.getKey());

                    SharedPreferences reminder_info = context.getSharedPreferences(newReminder.getNameTask() + newReminder.getCategory() + newReminder.getReminderDate().toString(), MODE_PRIVATE);
                    SharedPreferences.Editor reminder_info_edit = reminder_info.edit();

                    Intent alarmIntent = new Intent(context,AlarmReceiver.class);
                    alarmIntent.putExtra("taskname",newReminder.getNameTask());
                    alarmIntent.putExtra("taskpriority",newReminder.getPriority());
                    alarmIntent.putExtra("intentCode",SingletonDataFlow.getInstance().temp);
                    alarmIntent.putExtra("fbkeycode",newReminder.getKey());
                    PendingIntent pi = PendingIntent.getBroadcast(context,0,alarmIntent,PendingIntent.FLAG_UPDATE_CURRENT);


                    alarmManager.cancel(pi);
                    reminder_info_edit.clear();
                    reminder_info_edit.commit();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void readReminderOnce(final LinearLayout mainLayout, final Context context, final AlarmManager alarmManager){
        db.getReference("Reminder").addListenerForSingleValueEvent(new ValueEventListener() { // buat ngambil 1x
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lstOfCategorizedReminder.clear();
                ArrayList<Reminder> lstOfReminder = new ArrayList<>();
                getAllCategories(context);
                ArrayList<Reminder> lstOfHistoryReminder = new ArrayList<>();
                mainLayout.removeAllViews();
                LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearParams.setMargins(30,50,30,10);
                LinearLayout newLinearLayout = new LinearLayout(context);
                newLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                newLinearLayout.setLayoutParams(linearParams);
                TextView legend1 = new TextView(context);
                TextView legend2 = new TextView(context);
                TextView legend3 = new TextView(context);
                TextView legend4 = new TextView(context);

                LinearLayout.LayoutParams paramsText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1f);
                paramsText.setMargins(0,0,10,0);
                LinearLayout.LayoutParams paramsTextRight = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,1f);
                paramsTextRight.setMargins(10,0,0,0);

                legend1.setText("Important Urgent");
                legend1.setLayoutParams(paramsText);
                legend1.setGravity(Gravity.CENTER);
                legend1.setBackgroundResource(R.drawable.important_urgent_ractangle);

                legend2.setText("Important Not Urgent");
                legend2.setLayoutParams(paramsTextRight);
                legend2.setGravity(Gravity.CENTER);
                legend2.setBackgroundResource(R.drawable.important_not_urgent_ractangle);


                legend3.setText("Not Important Urgent");
                legend3.setLayoutParams(paramsText);
                legend3.setGravity(Gravity.CENTER);
                legend3.setBackgroundResource(R.drawable.not_important_urgent_ractangle);

                legend4.setText("Not Important Not Urgent");
                legend4.setLayoutParams(paramsTextRight);
                legend4.setGravity(Gravity.CENTER);
                legend4.setBackgroundResource(R.drawable.not_important_not_urgent_ractangle);

                newLinearLayout.addView(legend1);
                newLinearLayout.addView(legend2);

                LinearLayout.LayoutParams linearParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearParams2.setMargins(30,0,30,10);
                LinearLayout newLinearLayout2 = new LinearLayout(context);
                newLinearLayout2.setOrientation(LinearLayout.HORIZONTAL);
                newLinearLayout2.setLayoutParams(linearParams2);

                newLinearLayout2.addView(legend3);
                newLinearLayout2.addView(legend4);

                mainLayout.addView(newLinearLayout);
                mainLayout.addView(newLinearLayout2);
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Reminder newReminder = ds.getValue(Reminder.class);
                    if(!newReminder.getId().equalsIgnoreCase(SingletonDataFlow.getInstance().loggedInId)) continue;
                    newReminder.setKey(ds.getKey());

                    SharedPreferences reminder_info = context.getSharedPreferences(newReminder.getNameTask() + newReminder.getCategory() + newReminder.getReminderDate().toString(), MODE_PRIVATE);
                    SharedPreferences.Editor reminder_info_edit = reminder_info.edit();
                    String reminderExist = reminder_info.getString(newReminder.getNameTask() + newReminder.getCategory() + newReminder.getReminderDate().toString(),"");
                    if(reminderExist.equals("")){
                        Intent alarmIntent = new Intent(context,AlarmReceiver.class);
                        alarmIntent.putExtra("taskname",newReminder.getNameTask());
                        alarmIntent.putExtra("taskpriority",newReminder.getPriority());
                        alarmIntent.putExtra("intentCode",SingletonDataFlow.getInstance().temp);
                        alarmIntent.putExtra("fbkeycode",newReminder.getKey());
                        PendingIntent pi = PendingIntent.getBroadcast(context,0,alarmIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                        Calendar calendar = Calendar.getInstance();
                        DateFormat format = new SimpleDateFormat("yyyy/mm/dd hh:mm");
                        format.format(newReminder.getReminderDate());
                        calendar = format.getCalendar();
                        calendar.set(Calendar.SECOND,0);

                        alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pi);
                        reminder_info_edit.putString(newReminder.getNameTask() + newReminder.getCategory() + newReminder.getReminderDate().toString(),"true");
                        reminder_info_edit.commit();
                    }

                    for (DataSnapshot dsStopwatch : ds.child("stopwatchData").getChildren()){
                        newReminder.stopwatchData.add(dsStopwatch.getValue(Stopwatch.class));
                    }
                    Calendar calendar = Calendar.getInstance();
                    lstOfReminder.add(newReminder);
//                    if(newReminder.getReminderDate().compareTo(calendar.getTime()) < 0) {
//                        lstOfHistoryReminder.add(newReminder);
//                        Log.d("history","reminder");
//                    } else {
//                        lstOfReminder.add(newReminder);
//                        Log.d("nothistory","reminder");
//                    }
                }
                //Nampilin ini bila tidak ada task
                if(lstOfReminder.size() == 0) {
                    LinearLayout lin = new LinearLayout(context);
                    LinearLayout.LayoutParams full = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    TextView noRecord = new TextView(context);
                    noRecord.setText("No Schedule Task, Please Add One");
                    noRecord.setGravity(Gravity.CENTER);
                    noRecord.setTextColor(Color.WHITE);
                    noRecord.setTextSize(36);
                    LinearLayout.LayoutParams noRecords = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    noRecords.topMargin = 24;
                    noRecord.setLayoutParams(noRecords);
                    lin.setLayoutParams(full);
                    lin.setGravity(Gravity.CENTER);
                    lin.addView(noRecord);
                    mainLayout.addView(lin);
                }

                categorizingReminder(lstOfReminder, false);
                //categorizingReminder(lstOfHistoryReminder,true);
                ArrayList<Reminder> todayReminder = new ArrayList<>();
                for(Reminder rem : lstOfReminder) {
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    if(format.format(rem.getReminderDate()).equalsIgnoreCase(format.format(Calendar.getInstance().getTime()))){
                        todayReminder.add(rem);
                    }
                }


                if(todayReminder.size() > 0) {
                    TextView tv = new TextView(context);
                    tv.setGravity(Gravity.CENTER);
                    tv.setTextColor(Color.parseColor("#FFFFFF"));
                    tv.setText("Today");
                    tv.setTextSize(24);
                    mainLayout.addView(tv);
                    Collections.sort(todayReminder, new Comparator<Reminder>() {
                        @Override
                        public int compare(Reminder reminder, Reminder t1) {
                            return reminder.getReminderDate().compareTo(t1.getReminderDate());
                        }
                    });
                    for(Reminder rem : todayReminder) {
                        addToDoList(mainLayout,rem,context);
                    }
                    LinearLayout ll = new LinearLayout(context);
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    ll.setLayoutParams(param);
                    ll.setBackgroundResource(R.drawable.border_item);
                    param.setMargins(0,50,0,30);
                    mainLayout.addView(ll);
                }
                for(Categorized_Reminder cat_rem : lstOfCategorizedReminder){
                    TextView newTv = new TextView(context);
                    newTv.setGravity(Gravity.CENTER);
                    newTv.setText(cat_rem.getCategory());
                    newTv.setTextSize(24);
                    newTv.setTextColor(Color.parseColor("#FFFFFF"));
                    mainLayout.addView(newTv);

                    Collections.sort(cat_rem.getLstOfReminder(), new Comparator<Reminder>() {
                        @Override
                        public int compare(Reminder reminder, Reminder t1) {
                            return reminder.getReminderDate().compareTo(t1.getReminderDate());
                        }
                    });
                    for (Reminder rem : cat_rem.getLstOfReminder()){
                        addToDoList(mainLayout,rem,context);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addToDoHistoryList(final LinearLayout mainLayout,final Reminder reminder, Context context){ //dimensi dimensi an
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        final float scale = context.getResources().getDisplayMetrics().density;
        RelativeLayout newRow = new RelativeLayout(context); // bikin relative layout
        RelativeLayout.LayoutParams newRowLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        newRowLayout.leftMargin = 20;
        newRowLayout.rightMargin = 20;
        newRowLayout.bottomMargin = 10;

        TextView newTvDate = new TextView(context); // bikin textview buat tanggal
        newTvDate.setText(format.format(reminder.getReminderDate()));
        newTvDate.setWidth((int) (100 * scale + 0.5f));
        newTvDate.setMaxHeight((int) (80 * scale + 0.5f));
        newTvDate.setMinHeight((int) (80 * scale + 0.5f));
        newTvDate.setGravity(Gravity.CENTER);
//        newTvDate.setTextColor(Color.WHITE);
        newTvDate.setId(ViewCompat.generateViewId());

        final Button newTvStopwatch = new Button(context); // bikin textview buat stopwatch (nanti ganti simbol)
        RelativeLayout.LayoutParams stopwatchLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        stopwatchLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
        newTvStopwatch.setText(reminder.getPriority());
        newTvStopwatch.setMaxHeight((int) (80 * scale + 0.5f));
        newTvStopwatch.setMinHeight((int) (80 * scale + 0.5f));
        newTvStopwatch.setMinWidth((int) (80 * scale + 0.5f));
        newTvStopwatch.setMaxWidth((int) (80 * scale + 0.5f));
        newTvStopwatch.setId(ViewCompat.generateViewId());
        newTvStopwatch.setGravity(Gravity.CENTER);
//        newTvStopwatch.setTextColor(Color.WHITE);
        newTvStopwatch.setLayoutParams(stopwatchLayout);

        handler = new Handler();

//        newTvStopwatch.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                if(buttonId == newTvStopwatch.getId()){
//                    //TimeBuff += MillisecondTime;
//                    handler.removeCallbacks(runnable);
//                    String stopTime = "" + Minutes + ":"
//                            + String.format("%02d", Seconds) + ":"
//                            + String.format("%03d", MilliSeconds);
//                    Stopwatch stopwatch = new Stopwatch(stopTime);
//                    db.getInstance().getReference()
//                            .child("Reminder")
//                            .child(reminder.getKey())
//                            .child("stopwatchData")
//                            .push()
//                            .setValue(stopwatch);
//                    buttonId = -1234;
//                    reminder.stopwatchData.add(stopwatch);
//                }
//                else {
//                    buttonId = newTvStopwatch.getId();
//                    currButton = newTvStopwatch;
//                    StartTime = SystemClock.uptimeMillis();
//                    handler.postDelayed(runnable, 0);
//                }
//            }
//        });

        RelativeLayout.LayoutParams detailsLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        TextView newTvDetails = new TextView(context); // bikin text view buat detail
        detailsLayout.addRule(RelativeLayout.RIGHT_OF, newTvDate.getId());
        detailsLayout.addRule(RelativeLayout.LEFT_OF, newTvStopwatch.getId());
        newTvDetails.setMaxHeight((int) (80 * scale + 0.5f));
        newTvDetails.setMinHeight((int) (80 * scale + 0.5f));
        newTvDetails.setLayoutParams(detailsLayout);
        newTvDetails.setGravity(Gravity.CENTER);
//        newTvDetails.setTextColor(Color.WHITE);
        newTvDetails.setText(reminder.getNameTask());


        newTvDate.setBackgroundResource(R.drawable.border_item_leftradius); // buat nyambungin ke res drawable yang filenya border_item_leftradius
        newTvDetails.setBackgroundResource(R.drawable.border_home_item);
        if(reminder.getPriority().equals("Important Urgent")) {
            newTvStopwatch.setBackgroundResource(R.drawable.border_item_right_iu);
        } else if (reminder.getPriority().equals("Important Not Urgent")) {
            newTvStopwatch.setBackgroundResource(R.drawable.border_item_rightradius);
        } else if (reminder.getPriority().equals("Not Important Urgent")) {
            newTvStopwatch.setBackgroundResource(R.drawable.border_item_right_niu);
        } else {
            newTvStopwatch.setBackgroundResource(R.drawable.border_item_ninu);
        }


        newRow.addView(newTvDate);
        newRow.addView(newTvDetails);
        newRow.addView(newTvStopwatch);
        final Context finContext = context;
        final Reminder finRemDetails = reminder;
        newRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(finContext,ReminderDetails.class);
                i.putExtra("fromHistory","true");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                SingletonDataFlow.getInstance().remDetails = finRemDetails;
                finContext.startActivity(i);
            }
        });

        mainLayout.addView(newRow, newRowLayout);
    }


    public void addToDoList(final LinearLayout mainLayout,final Reminder reminder, Context context){ //dimensi dimensi an
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        final float scale = context.getResources().getDisplayMetrics().density;
        RelativeLayout newRow = new RelativeLayout(context); // bikin relative layout
        RelativeLayout.LayoutParams newRowLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        newRowLayout.leftMargin = 20;
        newRowLayout.rightMargin = 20;
        newRowLayout.bottomMargin = 10;

        TextView newTvDate = new TextView(context); // bikin textview buat tanggal
        newTvDate.setText(format.format(reminder.getReminderDate()));
        newTvDate.setWidth((int) (100 * scale + 0.5f));
        newTvDate.setMaxHeight((int) (80 * scale + 0.5f));
        newTvDate.setMinHeight((int) (80 * scale + 0.5f));
        newTvDate.setGravity(Gravity.CENTER);
//        newTvDate.setTextColor(Color.WHITE);
        newTvDate.setId(ViewCompat.generateViewId());

        final Button newTvStopwatch = new Button(context); // bikin textview buat stopwatch (nanti ganti simbol)
        RelativeLayout.LayoutParams stopwatchLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        stopwatchLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
        newTvStopwatch.setText("Start");
        newTvStopwatch.setMaxHeight((int) (80 * scale + 0.5f));
        newTvStopwatch.setMinHeight((int) (80 * scale + 0.5f));
        newTvStopwatch.setMinWidth((int) (80 * scale + 0.5f));
        newTvStopwatch.setMaxWidth((int) (80 * scale + 0.5f));
        newTvStopwatch.setId(ViewCompat.generateViewId());
        newTvStopwatch.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
        newTvStopwatch.setGravity(Gravity.CENTER);
//        newTvStopwatch.setTextColor(Color.WHITE);
        newTvStopwatch.setLayoutParams(stopwatchLayout);

        handler = new Handler();

        newTvStopwatch.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(buttonId == newTvStopwatch.getId()){
                    //TimeBuff += MillisecondTime;
                    handler.removeCallbacks(runnable);
                    String stopTime = "" + Minutes + ":"
                            + String.format("%02d", Seconds) + ":"
                            + String.format("%03d", MilliSeconds);
                    Stopwatch stopwatch = new Stopwatch(stopTime);
                    db.getInstance().getReference()
                            .child("Reminder")
                            .child(reminder.getKey())
                            .child("stopwatchData")
                            .push()
                            .setValue(stopwatch);
                    buttonId = -1234;
                    reminder.stopwatchData.add(stopwatch);
                }
                else {
                    buttonId = newTvStopwatch.getId();
                    currButton = newTvStopwatch;
                    StartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                }
            }
        });

        RelativeLayout.LayoutParams detailsLayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        TextView newTvDetails = new TextView(context); // bikin text view buat detail
        detailsLayout.addRule(RelativeLayout.RIGHT_OF, newTvDate.getId());
        detailsLayout.addRule(RelativeLayout.LEFT_OF, newTvStopwatch.getId());
        newTvDetails.setMaxHeight((int) (80 * scale + 0.5f));
        newTvDetails.setMinHeight((int) (80 * scale + 0.5f));
        newTvDetails.setLayoutParams(detailsLayout);
        newTvDetails.setGravity(Gravity.CENTER);
//        newTvDetails.setTextColor(Color.WHITE);
        newTvDetails.setText(reminder.getNameTask());


        newTvDate.setBackgroundResource(R.drawable.border_item_leftradius); // buat nyambungin ke res drawable yang filenya border_item_leftradius
        newTvDetails.setBackgroundResource(R.drawable.border_home_item);
        if(reminder.getPriority().equals("Important Urgent")) {
            newTvStopwatch.setBackgroundResource(R.drawable.border_item_right_iu);
        } else if (reminder.getPriority().equals("Important Not Urgent")) {
            newTvStopwatch.setBackgroundResource(R.drawable.border_item_rightradius);
        } else if (reminder.getPriority().equals("Not Important Urgent")) {
            newTvStopwatch.setBackgroundResource(R.drawable.border_item_right_niu);
        } else {
            newTvStopwatch.setBackgroundResource(R.drawable.border_item_ninu);
        }

        newRow.addView(newTvDate);
        newRow.addView(newTvDetails);
        newRow.addView(newTvStopwatch);
        final Context finContext = context;
        final Reminder finRemDetails = reminder;
        newRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(finContext,ReminderDetails.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                SingletonDataFlow.getInstance().remDetails = finRemDetails;
                finContext.startActivity(i);
            }
        });

        mainLayout.addView(newRow, newRowLayout);
    }

    public void deleteReminder(Reminder reminder, final Context context, final Activity activity) {
        db.getInstance().getReference()
                .child("Reminder")
                .child(reminder.getKey())
                .removeValue()
                .addOnSuccessListener(activity, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context,"Berhasil Menghapus Reminder",Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                });
    }

    public void updateReminder(Reminder rem, final Context context, final Activity activity) {
        SharedPreferences reminder_info = context.getSharedPreferences(rem.getNameTask() + rem.getCategory() + rem.getReminderDate().toString(), MODE_PRIVATE);
        SharedPreferences.Editor reminder_info_edit = reminder_info.edit();

        Intent alarmIntent = new Intent(context,AlarmReceiver.class);
        alarmIntent.putExtra("taskname",rem.getNameTask());
        alarmIntent.putExtra("taskpriority",rem.getPriority());
        alarmIntent.putExtra("intentCode",SingletonDataFlow.getInstance().temp);
        alarmIntent.putExtra("fbkeycode",rem.getKey());
        PendingIntent pi = PendingIntent.getBroadcast(context,0,alarmIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pi);
        reminder_info_edit.clear();
        reminder_info_edit.commit();
        db.getInstance().getReference()
                .child("Reminder")
                .child(rem.getKey())
                .setValue(rem)
                .addOnSuccessListener(activity, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Berhasil Mengupdate Reminder", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }
                });
    }

    private void categorizingReminder(ArrayList<Reminder> lstOfReminder, boolean isHistory){
        for(Reminder rem : lstOfReminder) {
            boolean exist = false;
            if(!isHistory) {
                for (Categorized_Reminder cat_rem : lstOfCategorizedReminder) {
                    if (cat_rem.getCategory().equals(rem.getCategory())) {
                        exist = true;
                        cat_rem.getLstOfReminder().add(rem);
                    }
                }
            } else {
                for (Categorized_Reminder cat_rem : lstOfHistoryCategorizedReminder) {
                    if (cat_rem.getCategory().equals(rem.getCategory())) {
                        exist = true;
                        cat_rem.getLstOfReminder().add(rem);
                    }
                }
            }
            if(!exist){
                if(!isHistory) {
                    ArrayList<Reminder> newlstOfReminder = new ArrayList<>();
                    newlstOfReminder.add(rem);
                    lstOfCategorizedReminder.add(new Categorized_Reminder(newlstOfReminder, rem.getCategory()));
                } else{
                    ArrayList<Reminder> newlstOfReminder = new ArrayList<>();
                    newlstOfReminder.add(rem);
                    lstOfHistoryCategorizedReminder.add(new Categorized_Reminder(newlstOfReminder, rem.getCategory()));
                }
            }
        }
    }

    public void login(final String id, final String password){
        // bawaan firebase dia ngeupdate terus
        db.getReference("Account").addListenerForSingleValueEvent(new ValueEventListener() { // buat ngambil 1x
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    if(id.equals("") || password.equals("")){
                        Toast.makeText(SingletonDataFlow.getInstance().context, "Tolong isi id dan password", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Account newacc = ds.getValue(Account.class);
                    newacc.setKey(ds.getKey());
                    if (newacc.getId().equalsIgnoreCase(id)) {
                        if (newacc.getPassword().equals(password)) {
                            SingletonDataFlow.getInstance().login_info_edit.putString("id",id);
                            SingletonDataFlow.getInstance().login_info_edit.commit();
                            SingletonDataFlow.getInstance().loggedInId = id;
                            Toast.makeText(SingletonDataFlow.getInstance().context, "Login Sukses", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(SingletonDataFlow.getInstance().context, MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            SingletonDataFlow.getInstance().context.startActivity(i);
                            return;
                        }
                    }
                }
                Toast.makeText(SingletonDataFlow.getInstance().context, "Login Gagal", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void writeNewId(final String id, final String password){
        if(id.length() < 4) {
            Toast.makeText(SingletonDataFlow.getInstance().context, "Panjang id minimal 4", Toast.LENGTH_SHORT).show();
            return;
        }
        if(password.length() < 6) {
            Toast.makeText(SingletonDataFlow.getInstance().context, "Panjang password minimal 6", Toast.LENGTH_SHORT).show();
            return;
        }
        db.getReference("Account").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean exist = false;
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    Account newacc = ds.getValue(Account.class);
                    exist = (newacc.getId().equalsIgnoreCase(id)) ? true : false;
                    if(exist == true){
                        break;
                    }
                }
                if(exist) {
                    Toast.makeText(SingletonDataFlow.getInstance().context, "Id sudah terpakai, silahkan gunakan id lain", Toast.LENGTH_SHORT).show();
                } else {
                    db.getInstance().getReference("Account").push().setValue(new Account(id,password));
                    Toast.makeText(SingletonDataFlow.getInstance().context, "Id telah berhasil dibuat", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(SingletonDataFlow.getInstance().context,LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    SingletonDataFlow.getInstance().context.startActivity(i);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        // db.getInstance().getReference("Account").push().setValue(new Account(id,password));
    }

    public void writeValueByReference(String reference, String value){
        db.getInstance().getReference(reference).setValue(value);
    }

    public Runnable runnable = new Runnable() {

        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);

            Minutes = Seconds / 60;

            Seconds = Seconds % 60;

            MilliSeconds = (int) (UpdateTime % 1000);

            currButton.setText("" + Minutes + ":"
                    + String.format("%02d", Seconds) + ":"
                    + String.format("%03d", MilliSeconds));

            handler.postDelayed(this, 0);
        }

    };
}
