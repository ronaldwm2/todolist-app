package com.example.personaltodolist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.personaltodolist.model.Reminder;
import com.example.personaltodolist.model.Stopwatch;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ReminderDetails extends AppCompatActivity {
    private String prioritySelected;
    private DatePickerDialog pickerDialog;
    private TimePickerDialog timePickerDialog;
    private Date date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_details);
        final Spinner priority = findViewById(R.id.priority_select_spinner);
        final EditText etName = findViewById(R.id.etName);
        final EditText etCategory = findViewById(R.id.etCategory);
        final EditText etDetail = findViewById(R.id.etDetail);
        final EditText etRemDate = findViewById(R.id.etDetailDate);
        final Button editBtn = findViewById(R.id.edit_btn);
        final Button deleteBtn = findViewById(R.id.delete_btn);
        final Button doneBtn = findViewById(R.id.done_button);
        final LinearLayout linearLayout = findViewById(R.id.layout_stopwatch);
        final Button oneDayBtn = findViewById(R.id.one_day_remind);
        final Button threeDayBtn = findViewById(R.id.three_day_remind);
        final Button oneWeekBtn = findViewById(R.id.one_week_remind);

        Intent intent = getIntent();

        if(intent.hasExtra("fromHistory")){
            editBtn.setVisibility(View.GONE);
            deleteBtn.setVisibility(View.GONE);
            doneBtn.setVisibility(View.GONE);
            etName.setEnabled(false);
            etCategory.setEnabled(false);
            etDetail.setEnabled(false);
            priority.setEnabled(false);
        }

        if(intent.hasExtra("fbkeycode")){
            SingletonDbHandler.getInstance().readSingleReminder(intent.getExtras().get("fbkeycode").toString());
        }

        etName.setText(SingletonDataFlow.getInstance().remDetails.getNameTask());
        etCategory.setText(SingletonDataFlow.getInstance().remDetails.getCategory());
        etDetail.setText(SingletonDataFlow.getInstance().remDetails.getDetail());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        etRemDate.setText(dateFormatter.format(SingletonDataFlow.getInstance().remDetails.getReminderDate()));
        date = SingletonDataFlow.getInstance().remDetails.getReminderDate();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.priority_list,
                android.R.layout.simple_spinner_dropdown_item
        );
        priority.setAdapter(adapter);
        List<String> data = Arrays.asList(getResources().getStringArray(R.array.priority_list));
        int idx = 0;
        for (String e: data) {
            Log.d("comparison",e);
            Log.d("Comparison-2",SingletonDataFlow.getInstance().remDetails.getPriority());
            if(e.equalsIgnoreCase(SingletonDataFlow.getInstance().remDetails.getPriority())){
                priority.setSelection(idx);
                prioritySelected = e;
                Log.d("TRUE BOS","RUE BOSSSSSS");
            }
            idx++;
        }

        for (Stopwatch e: SingletonDataFlow.getInstance().remDetails.stopwatchData) {
            TextView tv = new TextView(this);
            tv.setText(e.getTime());
            linearLayout.addView(tv);
            Log.d("STOPWATCHDATA",e.getTime());
        }

        etRemDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });


        priority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prioritySelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Reminder newRem = SingletonDataFlow.getInstance().remDetails;
                SingletonDbHandler.getInstance().deleteReminder(newRem,getApplicationContext(),ReminderDetails.this);
            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Reminder newRem = SingletonDataFlow.getInstance().remDetails;
                newRem.setCategory(etCategory.getText().toString());
                newRem.setDetail(etDetail.getText().toString());
                newRem.setNameTask(etName.getText().toString());
                newRem.setPriority(prioritySelected);
                if(date != null) {
                    newRem.setReminderDate(date);
                }
                SingletonDbHandler.getInstance().updateReminder(newRem,getApplicationContext(),ReminderDetails.this);
            }
        });
        Log.d("err",SingletonDataFlow.getInstance().remDetails.getKey());
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonDbHandler.getInstance().addHistoryReminder(SingletonDataFlow.getInstance().remDetails,SingletonDataFlow.getInstance().remDetails.getKey(),ReminderDetails.this);
            }
        });

        oneDayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("d");
                calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(format.format(calendar.getTime())) + 1);
                Reminder newRem = SingletonDataFlow.getInstance().remDetails;
                showTimeDialog(calendar, newRem);
            }
        });
        threeDayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("d");
                calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(format.format(calendar.getTime())) + 3);
                Reminder newRem = SingletonDataFlow.getInstance().remDetails;
                showTimeDialog(calendar, newRem);
            }
        });
        oneWeekBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("d");
                calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(format.format(calendar.getTime())) + 7);
                Reminder newRem = SingletonDataFlow.getInstance().remDetails;
                showTimeDialog(calendar, newRem);
            }
        });
    }

    private void showDateDialog(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(SingletonDataFlow.getInstance().remDetails.getReminderDate());

        pickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(i,i1,i2);
                date = newDate.getTime();
                showTimeDialog(newDate);
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        pickerDialog.show();
    }

    private void showTimeDialog(final Calendar calendar){
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                //Method ini dipanggil saat kita selesai memilih waktu di DatePicker
                calendar.set(Calendar.MINUTE,minute);
                calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                final EditText etRemDate = findViewById(R.id.etDetailDate);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                etRemDate.setText(dateFormatter.format(calendar.getTime()));
                date = calendar.getTime();
            }
        }, //Tampilkan jam saat ini ketika TimePicker pertama kali dibuka
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                //Cek apakah format waktu menggunakan 24-hour format
                DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

    private void showTimeDialog(final Calendar calendar, final Reminder rem){
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                //Method ini dipanggil saat kita selesai memilih waktu di DatePicker
                calendar.set(Calendar.MINUTE,minute);
                calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                final EditText etRemDate = findViewById(R.id.etDetailDate);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                etRemDate.setText(dateFormatter.format(calendar.getTime()));
                date = calendar.getTime();
                rem.setReminderDate(date);
                SingletonDbHandler.getInstance().updateReminder(rem,getApplicationContext(),ReminderDetails.this);
            }
        }, //Tampilkan jam saat ini ketika TimePicker pertama kali dibuka
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                //Cek apakah format waktu menggunakan 24-hour format
                DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            Intent i = new Intent(this,LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
