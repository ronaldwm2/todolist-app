package com.example.personaltodolist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.LinearLayout;

public class PriorityReminderListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_priority_reminder_list);

        LinearLayout mainLayout = findViewById(R.id.Priority_Reminder_Main_Layout);
        SingletonDbHandler.getInstance().filterReminderByImportance(mainLayout,this);
    }
}
