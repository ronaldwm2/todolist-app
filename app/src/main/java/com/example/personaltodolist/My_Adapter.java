package com.example.personaltodolist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.Collection;
import java.util.List;

public abstract class My_Adapter<T> extends BaseAdapter {

    private int customLayoutID;

    private Context myContext;

    private Collection<T> listValues;
    private T[] arrayValues;

    private final boolean listMode;
    private final boolean arrayMode;

    public My_Adapter(Context context,int layoutID, T[] values) {

        arrayMode=true;
        listMode=false;

        myContext=context;
        arrayValues=values;
        customLayoutID=layoutID;
    }

    public My_Adapter(Context context,int layoutID, Collection<T> values) {

        listMode=true;
        arrayMode=false;

        myContext=context;
        listValues=values;
        customLayoutID=layoutID;
    }

    @Override
    public int getCount() {

        if (listMode){

            return listValues.size();
        }
        else if (arrayMode){

            return arrayValues.length;
        }
        else return -1;
    }

    @Override
    public T getItem(int position) {

        if (listMode){

            return ((List<T>) listValues).get(position);
        }
        else if (arrayMode){

            return arrayValues[position];
        }
        else return null;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= LayoutInflater.from(myContext);

        View myView= inflater.inflate(customLayoutID,parent,false);

        T item=getItem(position);

        set(item,myView);

        return myView;
    }

    protected abstract void set(T item, View customView);
}
